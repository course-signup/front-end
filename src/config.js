const local = {
    debug: true,
    endpoint: 'http://localhost:3000/local'
}

const configs = {
    local,
    test: {
        ...local,
        debug: false
    },
    dev: {
        debug: true,
        endpoint : 'https://kbnon3w6t8.execute-api.eu-central-1.amazonaws.com/dev'
    },
    prod: {
        debug: false
    }
}

export default configs[process.env.STAGE]