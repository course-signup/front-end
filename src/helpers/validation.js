const EMAIL_REGEX = /^[^\s@]+@[^\s@]+\.[^\s@]+$/

export const isEmail = (value) => {
    if (!value) {
        return false
    }

    return !!value.match(EMAIL_REGEX)
}

export const validate = ({ courseId, email, matriculation }) => {
    const errors = { }

    if (!courseId) {
        errors.courseId = 'selected course is invalid'
    }

    if (!isEmail(email)) {
        errors.email = 'email is invalid'
    }

    if (!matriculation) {
        errors.matriculation = 'matriculation is invalid'
    }

    return errors
}