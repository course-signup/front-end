import config from '../config'

export default (message) => {
    if (config.debug !== true) {
        return
    }

    console.log(`[course-signup] ${message}`)
}