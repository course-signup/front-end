export const runAsync = async (func, setter) => {
    const result = await func()
    
    if (setter) {
        setter(result)
    }
}