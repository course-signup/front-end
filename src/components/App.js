import React, { useState, useEffect } from 'react'

import Alert from '@material-ui/lab/Alert'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import Snackbar from '@material-ui/core/Snackbar'
import TextField from '@material-ui/core/TextField'

import { getCourses, registerStudentForCourse } from '../api'
import { runAsync } from '../helpers/react-hooks'
import { formatCourse } from '../helpers/format'
import { validate } from '../helpers/validation'

import '../styles/App.css'

function App () {
    const [errors, setErrors] = useState({ })
    const [status, setStatus] = useState()

    const [courses, setCourses] = useState([])
    const [courseId, setCourseId] = useState(0)
    const [email, setEmail] = useState()
    const [matriculation, setMatriculation] = useState()

    const showStatusMessage = (message, severity = 'success') => {
        setStatus(
            <Alert
                elevation={6}
                variant="filled"
                severity={severity}
            >
                {message}
            </Alert>
        )
    }

    useEffect(() => {
        runAsync(
            async () => {
                const { data, error } = await getCourses()

                if (error) {
                    return showStatusMessage(error, 'error')
                }

                if (data) {
                    setCourses(data)
                }
            },
        )
    }, [])

    const registerStudent = (async () => {
        const errors = validate({ courseId, email, matriculation })
        setErrors(errors)

        if (Object.keys(errors).length > 0) {
            return
        }

        const { error, data } = await registerStudentForCourse({ courseId, email, matriculation })

        if (error) {
            return showStatusMessage(error, 'error')
        }

        if (!data) {
            const course = courses.find(course => course.id === courseId)
            if (course) {
                course.seatsAvailable -= 1
            }
        }

        showStatusMessage(data || 'registration successful')
    })
  
    return (
        <div>                
        <h1>COURSE SIGNUP</h1>

        <Snackbar
            key='topcenter'
            anchorOrigin={{ vertical: 'top', horizontal: "center" }}
            open={!!status}
            autoHideDuration={2500}
            onClose={() => setStatus()}>
            {status}
        </Snackbar>

        <br />

        <Container maxWidth="md">
            <Grid container spacing={3} justify="center" alignItems="center" style={{marginTop: '5%'}}>
                <form noValidate autoComplete="off">
                    <Grid item>
                        <InputLabel>Course</InputLabel>
                    </Grid>

                    <Grid item>
                        <Select
                            error={!!errors.courseId}
                            fullWidth
                            value={courseId}
                            onChange={e => setCourseId(e.target.value)}>
                            <MenuItem key={0} value={0}><em>Please select a course</em></MenuItem>

                            {courses.map((course) =>
                                <MenuItem key={course.id} value={course.id}>{formatCourse(course)}</MenuItem>
                            )}
                        </Select>
                    </Grid>

                    <Grid item>
                        <TextField
                            error={!!errors.email}
                            id="email"
                            label="Email"
                            placeholder="your email address"
                            fullWidth
                            type="email"
                            required
                            onChange={e => setEmail(e.target.value)} />
                    </Grid>

                    <Grid item>
                        <TextField
                            error={!!errors.matriculation}
                            id="matriculation"
                            label="Matriculation"
                            placeholder="your matriculation number"
                            fullWidth
                            required
                            onChange={e => setMatriculation(e.target.value)} />
                    </Grid>

                    <Grid container justify="flex-end">
                        <Grid item >
                            <Button onClick={registerStudent} variant="contained" color="primary" style={{ marginTop: 15 }}>Register</Button>
                        </Grid>
                    </Grid>
                </form>
            </Grid>
        </Container>
        
        </div>
    )
}

export default App