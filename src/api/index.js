import axios from 'axios'
import { STATUS_CODES } from './constants'
import debug from '../helpers/debug'
import config from '../config'

const { endpoint } = config

axios.defaults.validateStatus = () => true

export const getCourses = async () => {
    try {
        debug('getting list of courses ...')

        const { status, data } = await axios.get(`${endpoint}/courses`)

        debug(`received ${data.length} courses`)

        if (status !== STATUS_CODES.OK) {
            return { error: data || 'retrieval of courses failed' }
        }

        return { data }
    }
    catch(e) {
        return { error: e.message }
    }
}

export const registerStudentForCourse = async ({ courseId, email, matriculation }) => {
    try {
        debug(`registering student for course - ${JSON.stringify({ courseId, email, matriculation })} ...`)

        const { status, data } = await axios.post(
            `${endpoint}/courses/${courseId}/register`,
            { email, matriculation }
        )

        if (status !== STATUS_CODES.OK) {
            throw new Error(data || 'registration of student for course failed')
        }

        debug('registration succeeded')

        return { data }
    }
    catch(e) {
        return { error: e.message }
    }
}
